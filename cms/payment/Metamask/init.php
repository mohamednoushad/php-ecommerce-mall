
<head>
    <script src='https://cdnjs.cloudflare.com/ajax/libs/jquery/3.4.1/jquery.min.js'
        integrity='sha256-CSXorXvZcTkaix6Yvo6HppcZGetbYMGWSFlBw8HfCJo=' crossorigin='anonymous'></script>
<script src='../../assets/js/web3.min.js'></script>
<script src='../../assets/js/wallet.js'></script>

<!-- Stylesheets -->
    <link rel="stylesheet" href="../../assets/css/bootstrap.min.css">
    <link rel="stylesheet" href="../../assets/css/font-awesome.min.css">
    <link rel="stylesheet" href="../../assets/css/owl.carousel.min.css">
    <link rel="stylesheet" href="../../assets/css/owl.theme.default.min.css">
    <link rel="stylesheet" href="../../assets/css/jquery.bxslider.min.css">
    <link rel="stylesheet" href="../../assets/css/magnific-popup.css">
    <link rel="stylesheet" href="../../assets/css/rating.css">
    <link rel="stylesheet" href="../../assets/css/spacing.css">
    <link rel="stylesheet" href="../../assets/css/bootstrap-touch-slider.css">
    <link rel="stylesheet" href="../../assets/css/animate.min.css">
    <link rel="stylesheet" href="../../assets/css/tree-menu.css">
    <link rel="stylesheet" href="../../assets/css/select2.min.css">
    <link rel="stylesheet" href="../../assets/css/main.css">
    <link rel="stylesheet" href="../../assets/css/responsive.css">

    <!-- <script src="../../assets/js/jquery-2.2.4.min.js"></script> -->
    <script src="../../assets/js/bootstrap.min.js"></script>
    <script src="../../https://js.stripe.com/v2/"></script>
    <script src="../../assets/js/megamenu.js"></script>
    <script src="../../assets/js/owl.carousel.min.js"></script>
    <script src="../../assets/js/owl.animate.js"></script>
    <script src="../../assets/js/jquery.bxslider.min.js"></script>
    <script src="../../assets/js/jquery.magnific-popup.min.js"></script>
    <script src="../../assets/js/rating.js"></script>
    <script src="../../assets/js/jquery.touchSwipe.min.js"></script>
    <script src="../../assets/js/bootstrap-touch-slider.js"></script>
    <script src="../../assets/js/select2.full.min.js"></script>
   <!--  <script src="../../assets/js/custom.js"></script> -->


</head>
<?php
ob_start();
session_start();
include("../../admin/inc/config.php");
include("../../admin/inc/functions.php");
// Getting all language variables into array as global variable
$i=1;
$statement = $pdo->prepare("SELECT * FROM tbl_language");
$statement->execute();
$result = $statement->fetchAll(PDO::FETCH_ASSOC);                           
foreach ($result as $row) {
    define('LANG_VALUE_'.$i,$row['lang_value']);
    $i++;
}
?>

<?php require 'lib/init.php';
?>

<?php
$amount = $_POST['amount'];
?>

<div id='preloader'>
        <div id='status'></div>
</div>

<body>
<?php 
 echo"<script>
 $(document).ready( async function() {

    //loader start

    var str_num =  $amount ;
    const transaction_id =  await App.makePaymentInTokens(str_num);
    $.ajax ({
        type: 'POST',
        url: 'completion.php',
        data: {txn_id:transaction_id, amount:str_num},
        cache: false,
        success: function(data)
        {
        //loader close
        $('#preloader').hide();
        $('#status').hide();
        window.location.href = '../../payment_success.php';
        },
        error : function(error) {
            console.log(error);
        }
    });
    

 });
 </script>";
 exit();
?>
 </body>


